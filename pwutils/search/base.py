
class BaseSearchEngine(object):
    def get_connection(self):
        pass

    def get_results(self, query):
        pass